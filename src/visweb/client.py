import numpy as np
import requests
import warnings
import os
import sys
try:
    from .utils import serialize
except:
    from utils import serialize

def plot(data, compress=False, host='127.0.0.1', port=5050):
    if host in ('localhost', '127.0.0.1') and compress:
        warnings.warn(
            'Setting compress=True when sending on localhost may cause significant delay',
            RuntimeWarning
        )

    data = serialize(data, compress=compress)
    if sys.platform == 'win32' or host not in ('localhost', '127.0.0.1'):
        requests.post('http://%s:%d' % (host, port), data=data)
    else:
        fifo_path = f'/tmp/visweb_{port}.fifo'
        if not os.path.exists(fifo_path):
            os.mkfifo(fifo_path)
        with open(fifo_path, 'wb') as f:
            f.write(len(data).to_bytes(4, 'big'))
            f.write(data)
            f.flush()


class Plotter:

    def __enter__(self):
        self.plot_data = dict()

        def get_fn(plot_type):
            def fn(name, **kwargs):
                self.plot_data[name] = {'type': plot_type, **kwargs}
                return self
            return fn

        for plot_type in ['plot', 'scatter', 'image', 'mesh']:
            setattr(self, plot_type, get_fn(plot_type))

        return self

    def __exit__(self, *args):
        plot(self.plot_data)


if __name__ == '__main__':
    plot({
        'a': {
            'type': 'scatter',
            'pos': np.random.normal(0, 1, size=(100, 3))
        },
        'b': {
            'pos': np.random.normal(2, 1, size=(100, 3)),
        }
    })
