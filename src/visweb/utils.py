import pickle
from PIL import Image
from io import BytesIO
import numpy as np

def serialize(data, compress=False, depth=0):
    if isinstance(data, dict):
        data = {k: serialize(v, compress, depth + 1) for k, v in data.items()}
    elif isinstance(data, list):
        data = [serialize(e, compress, depth + 1) for e in data]
    elif isinstance(data, np.ndarray):
        if compress and data.ndim == 3 and \
            data.shape[0] > 3 and \
            data.shape[1] > 3 and \
            data.shape[2] == 3 and \
            data.dtype == np.uint8:

            # Assume data is image
            data = Image.fromarray(data)
            buf = BytesIO()
            data.save(buf, format='PNG')
            buf.seek(0)
            data = buf.read()

    if depth > 0:
        return data

    return pickle.dumps(data)

def deserialize(data, depth=0):
    if depth == 0:
        data = pickle.loads(data)

    if isinstance(data, dict):
        data = {k: deserialize(v, depth + 1) for k, v in data.items()}
    elif isinstance(data, list):
        data = [deserialize(e, depth + 1) for e in data]
    elif isinstance(data, bytes):
        try:
            buf = BytesIO(data)
            data = np.array(Image.open(buf))
        except:
            pass

    return data
