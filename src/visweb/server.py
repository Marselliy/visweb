import numpy as np
import pyqtgraph as pg
import pyqtgraph.opengl as gl

try:
    from .utils import deserialize
except:
    from utils import deserialize

import argparse
import os
import sys
import time
from http.server import BaseHTTPRequestHandler, HTTPServer

from PySide6 import QtCore, QtWidgets
from PySide6.QtWidgets import QWidget

parser = argparse.ArgumentParser(description="VisWeb server")
parser.add_argument("mode", help="Visualization mode (2d or 3d)")
parser.add_argument("--host", default="127.0.0.1", required=False)
parser.add_argument("--port", default=5050, required=False)
args = parser.parse_args()

app = QtWidgets.QApplication([""])
if args.mode == "3d":
    plot_widget = gl.GLViewWidget()
    plot_widget.opts["distance"] = 40
    plot_widget.setWindowTitle("3D view")
elif args.mode == "2d":
    plot_widget = pg.PlotWidget()
    plot_widget.getPlotItem().setAspectLocked(True)
    plot_widget.getViewBox().invertY(True)
else:
    raise ValueError("Unsupported mode '%s'" % args.mode)

w = 0


class HttpHandler(BaseHTTPRequestHandler):
    def do_POST(self):
        content_length = int(self.headers["Content-Length"])
        body = self.rfile.read(content_length)
        data = deserialize(body)

        w.updated.emit(data)

        self.send_response(200)
        self.send_header("Content-type", "text/plain")
        self.end_headers()


class HttpDaemon(QtCore.QThread):
    def __init__(self):
        super(HttpDaemon, self).__init__()

    def run(self):
        self._server = HTTPServer((args.host, args.port), HttpHandler)
        self._server.serve_forever()

    def stop(self):
        self._server.shutdown()
        self._server.socket.close()
        self.wait()


class FifoDaemon(QtCore.QThread):
    def __init__(self):
        super(FifoDaemon, self).__init__()
        self.stopped = False

    def run(self):
        path = f"/tmp/visweb_{args.port}.fifo"
        while not os.path.exists(path):
            time.sleep(0.1)
        with open(path, "rb") as f:
            while not self.stopped:
                l = f.read(4)
                if len(l) == 0:
                    time.sleep(0.01)
                else:
                    l = int.from_bytes(l, "big")
                    data = f.read(l)
                    data = deserialize(data)
                    w.updated.emit(data)

    def stop(self):
        self.stopped = True


class HttpWidget(QWidget):
    updated = QtCore.Signal(dict)

    def __init__(self):
        super(HttpWidget, self).__init__()

        self.plots = {}

        self.updated.connect(self.update_data)

        self.httpd = (
            FifoDaemon()
            if args.host in ("127.0.0.1", "localhost") and sys.platform != "win32"
            else HttpDaemon()
        )
        self.httpd.start()

        self.data_type_to_item = {
            "2d": {
                "plot": pg.PlotDataItem,
                "scatter": pg.ScatterPlotItem,
                "image": pg.ImageItem,
                "box": pg.PlotDataItem,
            },
            "3d": {
                "plot": gl.GLLinePlotItem,
                "scatter": gl.GLScatterPlotItem,
                "mesh": gl.GLMeshItem,
            },
        }

    def process_kwargs(self, kwargs):
        _args = kwargs.pop("args", [])

        t = kwargs.pop("type", "plot")

        if args.mode == "2d":
            if "color" in kwargs:
                col = np.array(kwargs.pop("color"))
                kwargs["pen_color"] = col
                kwargs["brush_color"] = col

            if "pen_color" in kwargs:
                col = kwargs.pop("pen_color")
                assert col.shape == (4,)
                kwargs["pen"] = pg.mkPen(col)

            if "brush_color" in kwargs:
                col = kwargs.pop("brush_color")
                assert col.shape == (4,)
                kwargs["brush"] = pg.mkBrush(col)

            if t == "plot":
                _args = [kwargs["pos"], *list(_args)]
                del kwargs["pos"]

            if t == "box":
                l, t, r, b = kwargs.pop("pos")
                _args = [
                    np.array([[l, t], [r, t], [r, b], [l, b], [l, t]]),
                    *list(_args),
                ]

        return _args, kwargs

    def update_data(self, data):
        # Sort types to show images first
        data = list(data.items())
        data = sorted(
            data,
            key=(
                lambda e: 1
                if "type" not in e[1]
                else (0 if e[1]["type"] == "image" else 1)
            ),
        )

        for k, v in data:
            t = v.get("type", "plot")
            if k in self.plots:
                plot_item = self.plots[k]
            else:
                try:
                    plot_item = self.data_type_to_item[args.mode][t]()
                except Exception:
                    raise NotImplementedError("Unsupported type: '%s'" % t)

                if args.mode == "3d":
                    plot_widget.addItem(plot_item)
                elif args.mode == "2d":
                    plot = plot_widget.getPlotItem()
                    plot.addItem(plot_item)
                self.plots[k] = plot_item

            try:
                a, kw = self.process_kwargs(v)
                if t == "image":
                    plot_item.setImage(*a, **kw)
                elif t == "mesh":
                    mesh_data = gl.MeshData(**kw)
                    plot_item.setMeshData(meshdata=mesh_data)
                    plot_item.setShader("shaded")
                else:
                    plot_item.setData(*a, **kw)
            except Exception as e:
                print(e)


w = HttpWidget()
w.resize(640, 480)
layout = QtWidgets.QGridLayout()

layout.addWidget(plot_widget)

if args.mode == "3d":
    plot_widget.addItem(gl.GLAxisItem())

w.setLayout(layout)
w.show()
w.resize(640, 480)

if __name__ == "__main__":
    app.exec_()
